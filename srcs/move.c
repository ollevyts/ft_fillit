/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/25 11:28:31 by ollevyts          #+#    #+#             */
/*   Updated: 2017/12/25 11:28:35 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_movex(char **tetr, int x, int y, int shift)
{
	while (y < 4 && shift < 0)
	{
		x = 0;
		while (x < 4)
		{
			if (tetr[x][y] == '#')
			{
				shift = y;
				break ;
			}
			x++;
		}
		y++;
	}
	y = shift;
	while (shift > 0 && y < 4)
	{
		x = 0;
		while (x < 4)
		{
			tetr[x][y - shift] = tetr[x][y];
			tetr[x++][y] = '.';
		}
		y++;
	}
}

void	ft_movey(char **tetr, int x, int y, int shift)
{
	while (x < 4 && shift < 0)
	{
		y = 0;
		while (y < 4)
		{
			if (tetr[x][y] == '#')
			{
				shift = x;
				break ;
			}
			y++;
		}
		x++;
	}
	x = shift;
	while (shift > 0 && x < 4)
	{
		y = 0;
		while (y < 4)
		{
			tetr[x - shift][y] = tetr[x][y];
			tetr[x][y++] = '.';
		}
		x++;
	}
}

void	ft_move_x(char **tetr)
{
	int	x;
	int	y;
	int shift;

	y = 0;
	x = 0;
	shift = -1;
	ft_movex(tetr, x, y, shift);
}

void	ft_move_y(char **tetr)
{
	int	x;
	int	y;
	int shift;

	x = 0;
	y = 0;
	shift = -1;
	ft_movey(tetr, x, y, shift);
}
