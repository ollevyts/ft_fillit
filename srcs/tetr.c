/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 15:02:19 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/21 15:02:21 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_checkt(char **tetr)
{
	int		x;
	int		y;
	int		i;

	x = -1;
	i = 0;
	while (++x < 4)
	{
		y = -1;
		while (++y < 4)
		{
			if (tetr[x][y] == '#')
			{
				if (y + 1 < 4 && tetr[x][y + 1] == '#')
					i++;
				if (y - 1 >= 0 && tetr[x][y - 1] == '#')
					i++;
				if (x + 1 < 4 && tetr[x + 1][y] == '#')
					i++;
				if (x - 1 >= 0 && tetr[x - 1][y] == '#')
					i++;
			}
		}
	}
	return ((i == 6 || i == 8) ? 1 : 0);
}

int			*ft_ctoi(char **tetr)
{
	int		*s;
	int		x;
	int		y;
	int		i;

	s = (int *)malloc(sizeof(int) * 16);
	x = 0;
	i = 0;
	while (x < 4)
	{
		y = 0;
		while (y < 4)
		{
			if (tetr[x][y] == '.')
				s[i] = 0;
			if (tetr[x][y] == '#')
				s[i] = 1;
			if (tetr[x][y] == '\n')
				y++;
			i++;
			y++;
		}
		x++;
	}
	return (s);
}
