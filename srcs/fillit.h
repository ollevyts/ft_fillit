/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fil.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 19:17:27 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/21 19:17:28 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include "../libft/libft.h"
# define RET(r) if (r) return (0);

typedef struct	s_map
{
	int			**matrix;
	int			**copy;
	int			figure;
	int			size;
	int			sqrt;
}				t_map;

void			ft_copy_map(t_map *map);
int				ft_calc_map(t_map *map, int ok);
void			ft_fillit_logic(t_map *map);
int				ft_check_col_min(t_map *map, int a, int i);
int				ft_check_col_max(t_map *map, int a, int i);
int				ft_minimal(int *f, t_map *map);
int				ft_maximum(int *f, t_map *map);
int				ft_check_logic(int *f, t_map *map);
int				ft_check_column(t_map *map, int a, int b);
int				ft_fill(t_map *map, int a);
void			ft_copy_line(t_map *map, int a);
void			ft_map_min(t_map *map);
int				ft_count_one(t_map *map, int lenght);
int				ft_count_two(t_map *map, int lenght);
void			ft_reinimapmin(t_map *map);
int				ft_sqrt_(int nb);
void			ft_calc_size(t_map *map);
void			ft_reinimap(t_map *map);
void			initialization(int **matrix, int figure);
int				ft_err(char *av);
int				ft_start(char *argv, int i, int j, int bap);
void			ft_movex(char **tetr, int x, int y, int shift);
void			ft_movey(char **tetr, int x, int y, int shift);
void			ft_move_x(char **tetr);
void			ft_move_y(char **tetr);
void			ft_move_line(t_map *map, int a, int how_move);
int				ft_check_move(t_map *map, int a);
int				ft_move_logic(t_map *map, int a);
void			fill_results(t_map *map, char *print_map);
void			ft_print_result(t_map *map);
int				ft_flsize(char *fl);
char			*ft_reader(char *fl);
void			ft_matrix_resize(t_map *map, int a, int b, int c);
int				ft_chk(char *mal);
int				ft_check(char *mal, int p, int h, int n);
int				ft_countsq(char *mal);
char			**ft_cut(char *mal);
int				ft_checkt(char **tetr);
int				*ft_ctoi(char **tetr);

#endif
