/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ini.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 15:28:54 by apakhomo          #+#    #+#             */
/*   Updated: 2017/12/23 15:28:55 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_map_min(t_map *map)
{
	int		**new;
	int		a;
	int		b;
	int		c;
	int		d;

	new = (int**)malloc(sizeof(int*) * (map->figure + 1));
	a = 0;
	while (a < map->figure)
	{
		new[a] = (int*)malloc(sizeof(int) * map->sqrt);
		b = 0;
		d = 0;
		ft_bzero(new[a], map->sqrt * 4);
		while (d < map->sqrt)
		{
			c = 0;
			while (c < map->size)
				new[a][d++] = map->matrix[a][b + c++];
			b = b + 4;
		}
		a++;
	}
	new[a] = NULL;
	map->copy = new;
}

int		ft_count_one(t_map *map, int lenght)
{
	int a;
	int b;
	int count;

	a = 0;
	count = 0;
	while (a < map->figure)
	{
		b = 0;
		while (b < lenght)
		{
			if (map->matrix[a][b] == 1)
				count++;
			b++;
		}
		a++;
	}
	return (count);
}

int		ft_count_two(t_map *map, int lenght)
{
	int a;
	int b;
	int count;

	a = 0;
	count = 0;
	while (a < map->figure)
	{
		b = 0;
		while (b < lenght)
		{
			if (map->copy[a][b] == 1)
				count++;
			b++;
		}
		a++;
	}
	return (count);
}

void	ft_reinimapmin(t_map *map)
{
	int counta;
	int countb;

	counta = ft_count_one(map, 16);
	countb = ft_count_two(map, map->sqrt);
	if (counta == countb)
		map->matrix = map->copy;
	else
	{
		map->size++;
		map->sqrt = map->size * map->size;
		ft_map_min(map);
		countb = ft_count_two(map, map->sqrt);
		if (counta == countb)
			map->matrix = map->copy;
		else
		{
			map->size++;
			map->sqrt = map->size * map->size;
			ft_map_min(map);
			countb = ft_count_two(map, map->sqrt);
			if (counta == countb)
				map->matrix = map->copy;
		}
	}
}
