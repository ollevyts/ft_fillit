/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resize.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 13:55:27 by apakhomo          #+#    #+#             */
/*   Updated: 2017/12/23 13:55:28 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_matrix_resize(t_map *map, int a, int b, int c)
{
	int		**new;
	int		d;

	new = (int**)malloc(sizeof(int*) * (map->figure + 1));
	a = 0;
	while (a < map->figure)
	{
		new[a] = (int*)malloc(sizeof(int) * (map->size + 1) * (map->size + 1));
		b = 0;
		d = 0;
		ft_bzero(new[a], (map->size + 1) * (map->size + 1) * 4);
		while (b < map->sqrt)
		{
			c = 0;
			while (c < map->size && d < map->sqrt)
				new[a][b + c++] = map->matrix[a][d++];
			b = b + map->size + 1;
		}
		a++;
	}
	new[a] = NULL;
	map->matrix = new;
	map->size = map->size + 1;
	map->sqrt = map->size * map->size;
}
