/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 13:49:49 by apakhomo          #+#    #+#             */
/*   Updated: 2017/12/23 13:49:50 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_copy_map(t_map *map)
{
	int		a;
	int		b;

	map->copy = (int**)malloc(sizeof(int*) * (map->figure + 1));
	a = 0;
	while (a < map->figure)
	{
		map->copy[a] = (int*)malloc(sizeof(int) * (map->sqrt));
		b = 0;
		while (b < map->sqrt)
		{
			map->copy[a][b] = map->matrix[a][b];
			b++;
		}
		a++;
	}
	map->copy[a] = NULL;
}

int		ft_calc_map(t_map *map, int ok)
{
	int a;

	a = 0;
	while (a < map->figure)
	{
		if (ok == 1)
		{
			ft_copy_line(map, a);
			if (a-- > 0)
				ok = ft_move_logic(map, a);
		}
		if (ft_fill(map, a) == 0)
			a++;
		else
			ok = ft_move_logic(map, a);
		if (ok == 1)
		{
			ft_copy_line(map, a);
			if (a-- > 0)
				ok = ft_move_logic(map, a);
		}
		if (ok == 2)
			return (ok);
	}
	return (0);
}

void	ft_fillit_logic(t_map *map)
{
	int	res;
	int a;
	int ok;

	a = 0;
	ok = 0;
	ft_copy_map(map);
	res = ft_calc_map(map, ok);
	if (res == 2)
	{
		ft_matrix_resize(map, 0, 0, 0);
		ft_copy_map(map);
		res = ft_calc_map(map, ok);
		if (res == 2)
		{
			ft_matrix_resize(map, 0, 0, 0);
			ft_copy_map(map);
			res = ft_calc_map(map, ok);
		}
	}
	ft_print_result(map);
}
