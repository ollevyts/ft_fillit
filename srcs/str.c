/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 12:04:20 by ollevyts          #+#    #+#             */
/*   Updated: 2017/12/23 12:04:43 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_chk(char *mal)
{
	int	i;

	i = 0;
	while (mal[i])
	{
		if ((i == 4 || i == 20) && mal[i] != '\n')
			return (0);
		i++;
	}
	return (1);
}

int			ft_check(char *mal, int p, int h, int n)
{
	size_t	i;

	i = 0;
	while (i <= ft_strlen(mal))
	{
		if (mal[i] == '.')
			p++;
		if (mal[i] == '#')
			h++;
		if (i % 5 == 0 && mal[i] == '\n')
			n++;
		else if (i % 21 == 0 && mal[i] == '\n')
			n++;
		i++;
		if (i == 19 && mal[i] == 0)
			return (0);
	}
	if (!(ft_chk(mal)))
		return (0);
	RET((mal[i - 1] == '\0' && mal[i - 2] == '\n' && mal[i - 3] == '\n'));
	if (p < 12 || h < 4)
		return (0);
	if (p % 12 == 0 && h % 4 == 0 && n % 5 == 0)
		return (1);
	return (0);
}

int			ft_countsq(char *mal)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (mal[j])
	{
		if (j % 21 == 0)
			i++;
		j++;
	}
	if (i > 26)
		return (0);
	return (i);
}

char		**ft_cut(char *mal)
{
	int		i;
	int		j;
	char	**tetr;

	tetr = (char **)malloc(sizeof(char *) * 4 + 1);
	i = 0;
	j = 0;
	while (i < 4)
	{
		tetr[i] = (char *)malloc(sizeof(char) * 4 + 1);
		while (j < 4)
		{
			if (*mal == '\n')
				mal++;
			tetr[i][j] = *mal;
			j++;
			mal++;
		}
		tetr[i][j] = '\0';
		i++;
		j = 0;
	}
	return (tetr);
}
