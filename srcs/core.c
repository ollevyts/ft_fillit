/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 14:11:52 by apakhomo          #+#    #+#             */
/*   Updated: 2017/12/23 14:11:52 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_check_column(t_map *map, int a, int b)
{
	int i;

	i = 0;
	while (i < a)
	{
		if (map->copy[i][b] == 0)
			i++;
		else
			return (1);
	}
	return (0);
}

int		ft_fill(t_map *map, int a)
{
	int b;
	int check_count;

	b = 0;
	check_count = 0;
	while (b < map->sqrt)
	{
		if (map->copy[a][b] == 1)
		{
			if (ft_check_column(map, a, b) == 0)
				check_count++;
		}
		b++;
	}
	if (check_count == 4)
		return (0);
	else
		return (1);
}

void	ft_copy_line(t_map *map, int a)
{
	int i;

	i = 0;
	while (i < map->sqrt)
	{
		map->copy[a][i] = map->matrix[a][i];
		i++;
	}
}
