/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 11:49:09 by ollevyts          #+#    #+#             */
/*   Updated: 2017/12/23 11:50:46 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_err(char *av)
{
	if (!(ft_check(ft_reader(av), 0, 0, 0)))
	{
		ft_putstr("error\n");
		return (1);
	}
	return (0);
}

int		ft_start(char *argv, int i, int j, int bap)
{
	char	*c;
	int		**a;
	int		y;
	char	**s;

	c = ft_reader(argv);
	j = ft_countsq(ft_reader(argv));
	a = malloc(j);
	y = 0;
	i = j;
	while (j > 0)
	{
		s = ft_cut(&c[bap]);
		ft_move_x(s);
		ft_move_y(s);
		if (!(ft_checkt(s)))
			return (-1);
		a[y++] = ft_ctoi(s);
		bap = bap + 21;
		j--;
	}
	if (i > 0)
		initialization(a, i);
	return (i);
}

int		main(int argc, char **argv)
{
	if (argc != 2)
	{
		ft_putstr("usage: ./fillit source_file\n");
		return (1);
	}
	if (ft_err(argv[1]) == 1)
		return (1);
	if (ft_start(argv[1], 0, 0, 0) < 1)
	{
		ft_putstr("error\n");
		return (1);
	}
	return (0);
}
