/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_move_logic.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 14:16:57 by apakhomo          #+#    #+#             */
/*   Updated: 2017/12/23 14:16:58 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int	ft_check_col_min(t_map *map, int a, int i)
{
	int x;
	int y;
	int z;

	x = ((i + 1) * map->size);
	y = x - map->size;
	z = 1;
	while (y < x)
	{
		if (map->copy[a][y] == 1)
			return (z);
		y++;
		z++;
	}
	return (0);
}

int	ft_check_col_max(t_map *map, int a, int i)
{
	int x;
	int y;
	int z;

	x = ((i + 1) * map->size) - 1;
	y = i * map->size - 1;
	z = map->size;
	while (z > 0)
	{
		if (map->copy[a][x] == 1)
			return (z);
		x--;
		z--;
	}
	return (0);
}

int	ft_minimal(int *f, t_map *map)
{
	int a;
	int min;

	a = 0;
	min = map->size;
	while (a < map->size * 2)
	{
		if (f[a] > 0)
		{
			if (min > f[a])
				min = f[a];
		}
		a += 2;
	}
	return (min);
}

int	ft_maximum(int *f, t_map *map)
{
	int a;
	int max;

	a = 1;
	max = 1;
	while (a < map->size * 2)
	{
		if (f[a] > 0)
		{
			if (max < f[a])
				max = f[a];
		}
		a += 2;
	}
	return (max);
}

int	ft_check_logic(int *f, t_map *map)
{
	int min;
	int max;

	min = ft_minimal(f, map);
	max = ft_maximum(f, map);
	if (max == map->size)
		return (map->size + 1 - min);
	else if (max < map->size)
		return (1);
	return (0);
}
