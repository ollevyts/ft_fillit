/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_results.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 14:01:24 by apakhomo          #+#    #+#             */
/*   Updated: 2017/12/23 14:01:24 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	fill_results(t_map *map, char *print_map)
{
	int		a;
	int		b;

	a = 0;
	while (a < map->figure)
	{
		b = 0;
		while (b < map->sqrt)
		{
			if (map->copy[a][b] == 1)
				print_map[b] = 65 + a;
			b++;
		}
		a++;
	}
	b = 0;
	while (b < map->sqrt)
	{
		if (print_map[b] < 65)
			print_map[b] = '.';
		b++;
	}
}

void	ft_print_result(t_map *map)
{
	int		a;
	int		b;
	int		c;
	char	*print_map;

	print_map = (char*)malloc(sizeof(char) * ((map->sqrt * map->figure) + 1));
	fill_results(map, print_map);
	a = 0;
	c = 0;
	while (a < map->size)
	{
		b = 0;
		while (b < map->size)
		{
			ft_putchar(print_map[c++]);
			b++;
		}
		ft_putchar('\n');
		a++;
	}
	free(print_map);
	print_map = NULL;
}
