/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_logic.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 14:14:16 by apakhomo          #+#    #+#             */
/*   Updated: 2017/12/23 14:14:16 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_move_line(t_map *map, int a, int how_move)
{
	int i;

	i = map->sqrt - 1;
	while (i > how_move)
	{
		map->copy[a][i] = map->copy[a][i - how_move];
		i--;
	}
	map->copy[a][i] = map->copy[a][i - how_move];
	while (how_move > 0)
	{
		map->copy[a][how_move - 1] = 0;
		how_move--;
	}
}

int		ft_check_move(t_map *map, int a)
{
	int f[map->size * 2];
	int i;
	int j;
	int k;

	i = 0;
	k = 0;
	ft_bzero(f, map->size * 2 * 4);
	while (i < map->size)
	{
		f[k++] = ft_check_col_min(map, a, i);
		f[k++] = ft_check_col_max(map, a, i);
		i++;
	}
	k = ft_check_logic(f, map);
	j = (map->sqrt) - 1;
	while (map->copy[a][j] != 1)
		j--;
	if (k > 0 && k + j < map->sqrt)
		return (k);
	return (0);
}

int		ft_move_logic(t_map *map, int a)
{
	int how_move;

	how_move = ft_check_move(map, a);
	if (how_move > 0)
	{
		ft_move_line(map, a, how_move);
		return (0);
	}
	if (how_move == 0 && a == 0)
		return (2);
	if (how_move == 0)
		return (1);
	return (0);
}
