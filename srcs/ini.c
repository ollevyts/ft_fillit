/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ini.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 15:28:54 by apakhomo          #+#    #+#             */
/*   Updated: 2017/12/23 15:28:55 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_sqrt_(int nb)
{
	int temp;

	temp = 1;
	while (temp * temp < nb)
		temp++;
	return (temp);
}

void		ft_calc_size(t_map *map)
{
	int	size;

	size = ft_sqrt_(map->figure * 4);
	if (size * size < map->figure * 4)
		size++;
	map->size = size;
}

void		ft_reinimap(t_map *map)
{
	int		**new;
	int		a;
	int		b;
	int		c;
	int		d;

	new = (int**)malloc(sizeof(int*) * (map->figure + 1));
	a = 0;
	while (a < map->figure)
	{
		new[a] = (int*)malloc(sizeof(int) * map->sqrt);
		b = 0;
		d = 0;
		ft_bzero(new[a], map->sqrt * 4);
		while (b < map->sqrt)
		{
			c = 0;
			while (c < 4 && d < 16)
				new[a][b + c++] = map->matrix[a][d++];
			b = b + map->size;
		}
		a++;
	}
	new[a] = NULL;
	map->matrix = new;
}

void		initialization(int **matrix, int figure)
{
	t_map	mapp;
	t_map	*map;

	map = &mapp;
	map->matrix = matrix;
	map->figure = figure;
	ft_calc_size(map);
	map->sqrt = map->size * map->size;
	if (map->size > 4)
		ft_reinimap(map);
	if (map->size < 4)
	{
		ft_map_min(map);
		ft_reinimapmin(map);
	}
	ft_fillit_logic(map);
}
