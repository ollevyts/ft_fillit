/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 11:56:21 by ollevyts          #+#    #+#             */
/*   Updated: 2017/12/23 11:56:23 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#define BUF_SIZE 1

int			ft_flsize(char *fl)
{
	int		i;
	int		fd;
	char	buf[BUF_SIZE];

	i = 0;
	if (!(fd = open(fl, O_RDONLY)))
	{
		ft_putstr("Open Error");
		return (0);
	}
	while (read(fd, buf, BUF_SIZE))
		i++;
	return (i);
}

char		*ft_reader(char *fl)
{
	int		i;
	char	*mal;
	char	buf[BUF_SIZE];
	int		fd;

	if (fl == NULL)
		return (NULL);
	i = 0;
	mal = (char *)malloc(sizeof(char) * (ft_flsize(fl) * BUF_SIZE) + 1);
	if (mal == NULL)
		return (NULL);
	if (!(fd = open(fl, O_RDONLY)))
	{
		ft_putstr("Open Error");
		free(mal);
		return (NULL);
	}
	while (read(fd, buf, BUF_SIZE))
	{
		mal[i] = buf[0];
		i++;
	}
	mal[i] = '\0';
	return (mal);
}
